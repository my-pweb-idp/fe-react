// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
  apiKey: "AIzaSyDBiXMg8ivcQed6VGuzUHukiQZlOxoQP5U",
  authDomain: "pweb-348515.firebaseapp.com",
  projectId: "pweb-348515",
  storageBucket: "pweb-348515.appspot.com",
  messagingSenderId: "616356823634",
  appId: "1:616356823634:web:f3c85552ea4985f311558b",
  measurementId: "G-84XQZC9S5S"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)

