import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styling/general.scss';

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// root.use(express.static(__dirname)); //here is important thing - no static directory, because all static :)


// root.get("/*", function (req, res) {
//   res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
// })

reportWebVitals();
