import { createUserWithEmailAndPassword, sendPasswordResetEmail, signInWithEmailAndPassword, signOut, updatePassword, updateEmail, updateProfile,  GoogleAuthProvider, signInWithPopup, getIdToken} from 'firebase/auth'
import React, { useContext, useState, useEffect } from 'react'
import { auth } from '../firebase-config'


const AuthContext = React.createContext()

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({ children }) {

    const [currentUser, setCurrentUser] = useState()
    const [loading, setLoading] = useState(true)
    const [token, setToken]=useState()
    const provider = new GoogleAuthProvider()

    function signInWithGoogle() {
      return signInWithPopup(auth, provider)
      .then((result) => {
        console.log(result)
      }).catch((error) => {
        console.log(error)
      })
    }

    function signup(email, password) {
      return createUserWithEmailAndPassword(auth, email, password)
    }

    function login(email, password) {
      return signInWithEmailAndPassword(auth, email, password);
    }

    function logout() {
      return signOut(auth)
    }

    function updateUserEmail(email) {
      return updateEmail(currentUser, email)
    }

    function updateUserPassword(email) {
      return updatePassword(currentUser, email)
    }

    function updateUserProfile(displayName, photoUrl) {
      console.log("user"+ displayName)
      return updateProfile(currentUser, {displayName, photoURL: photoUrl})
    }

    function resetPassword(email) {
      return sendPasswordResetEmail(auth, email)
    }


    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(user => {
          if (user) {
            setCurrentUser(user)
            setLoading(false)
            setToken(getIdToken(user));
          } 
            
        })
        
        return unsubscribe
    }, [])


    const value = {
        currentUser,
        signup,
        signInWithGoogle,
        login,
        logout,
        resetPassword,
        updateUserEmail,
        updateUserPassword,
        updateUserProfile,
        token
    }
  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  )
}
