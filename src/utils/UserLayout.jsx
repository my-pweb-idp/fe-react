// import { useAuth0 } from "@auth0/auth0-react";
import React from "react"
// import { useNavigate } from "react-router-dom";
// import { authSettings } from "../AuthSettings";
import UserHeader from "./UserHeader"
import UserFooter from "./UserFooter"

const UserLayout = ({ children }) => {
  // const { user } = useAuth0();
  // const navigate = useNavigate();

  // /* inseamna ca are admin ca si rol, deci nu poate vedea partea de utilizator */
  // useEffect(() => {
  //   if (user && user[authSettings.rolesKey].length === 1) {
  //     navigate("/books");
  //   }
  // }, [user]);

  return (
    <div>
      <UserHeader />
      <div>{children}</div>
      <UserFooter />
    </div>
  );
};

export default UserLayout;
