import React from "react"

function Footer() {
  return (
      <footer className="py-4 bg-light" style={{ color: "green", 
      textAlign: "center", 
      marginTop: "300px" }}>
          <div className="container"><p className="m-0 text-center text-black">Copyright © BookSharing App 2022</p></div>
      </footer>
    
  );
}
export default Footer;