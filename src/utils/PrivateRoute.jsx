import React from "react";
import { Navigate } from 'react-router-dom'
import { useAuth } from "../contexts/AuthContext";
import UserLayout from "./UserLayout";


export default function PrivateRoute({ children }) {
  const { currentUser } = useAuth();

  return currentUser ? <UserLayout>{children}</UserLayout> : <Navigate to="/login" />;
};

