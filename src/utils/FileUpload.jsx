import http from "../http-common";

class FileUpload {
  upload(email, rand, file, onUploadProgress) {
    let formData = new FormData();

    formData.append("files", file);
    formData.append("product", rand)
    formData.append("email", email)
    return http.post("/postImage", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
      onUploadProgress,
    });
  }

  getFiles() {
    return http.get("/files");
  }
}

export default new FileUpload();