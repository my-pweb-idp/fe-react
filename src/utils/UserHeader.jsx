import React, {useRef,useState} from "react"
import { Button, Nav, Navbar, NavDropdown, Form, FormControl, Container } from "react-bootstrap";
import { Link, useNavigate} from "react-router-dom"
import { BsFillPersonFill, BsPencilSquare, BsGearFill, BsBoxArrowRight, BsPlus } from "react-icons/bs";
import { ImBooks } from "react-icons/im";
import { RiHome2Line } from "react-icons/ri";
import logo from "../assets/logo11.png"
import { useAuth } from "../contexts/AuthContext"

const UserHeader = () => {
  // const { logout} = useAuth()
  const [error, setError] = useState('')
  const navigate = useNavigate()
  const textRef = useRef()

  function siteSelectedCallback() {
    
    navigate('/books/' + textRef.current.value)
  }

  async function handleLogout() {
    setError('')

    try {
      // await logout()
      navigate('/login')
    } catch {
      setError('Failed to log out')
    }
  }

  return (
    <Navbar className="navbar navbar-light bg-light" expand="lg">
      <Container fluid>
        <Navbar.Brand className="mr-3" as={Link} to="/"><img src={logo} width="156" height="45" alt="Weblib logo"/></Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="m-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          > 
            <Form className="d-flex justify-content-center">
              <FormControl
                type="search"
                placeholder="Search book by title"
                ref={textRef}
                className="me-2"
                aria-label="Search"
              />
              <Button variant="outline-success" onClick={()=>{siteSelectedCallback()}}>Search</Button>
            </Form>
          </Nav>
          
          <Nav>
            <Nav.Link as={Link} to="/addBook"><BsPlus size={40} /></Nav.Link>
            <Nav.Link as={Link} to="/books"><ImBooks  size={40}/></Nav.Link>
            <Nav.Link as={Link} to="/applications"><RiHome2Line  size={40}/></Nav.Link>
            
            
            <NavDropdown
              id="nav-dropdown-dark-example"
              title={<div style={{display: "inline-block"}}><BsFillPersonFill size={40} /></div>}
              menuVariant="light"
              as={Link} to="/profile" 
            >
              <NavDropdown.Item as={Link} to="/profile"><BsFillPersonFill />  My Profile</NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/userBooks"><ImBooks /> My Posts</NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/updateProfile"><BsPencilSquare /> Edit Profile</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3"><BsGearFill />  Settings</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item onClick={handleLogout}><BsBoxArrowRight />  Sign Out</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default UserHeader;
