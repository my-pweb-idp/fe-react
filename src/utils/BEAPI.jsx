import http from "../http-common";

class BEAPI {
  
  saveOrUpdateProfile(email, token) {

    const profileDTO = {"email": email }
    return http.post("/saveOrUpdateProfile", profileDTO, {
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
      }},
      {withCredentials: true});
  }

  updateProfile(email, phone, name) {
    const profileDTO = {"email": email,
                        "phoneNumber": phone,
                        "name": name }
  
    return http.post("/updateProfile", profileDTO, {
      headers: {
        "Content-Type": "application/json",
      }});
  }

  getProfile(email) {
    return http.get("/profile", { params: { email: email } });
  }

  addProduct(email, rand, title, description, locationLat, locationLon, type, author) {
    console.log("Dsadsadasdsa")
    const productDTO = {
            "email": email, 
            "title": title, 
            "description": description, 
            "typeName": type, 
            "author": author,
            "locationLat": locationLat, 
            "locationLong": locationLon, 
            "imagePath": rand,
          }
  
    return http.post("/postProduct", productDTO, {
      headers: {
        "Content-Type": "application/json",
      }});
  }

  updateProduct(email, id, title, description, locationLat, locationLon, type, author) {
    const productDTO = {
            "email": email, 
            "title": title, 
            "description": description, 
            "typeName": type, 
            "author": author,
            "locationLat": locationLat, 
            "locationLong": locationLon, 
            "imagePath": ""
          }
  
    return http.post("/updateProduct", productDTO,{ params: { id: id}}, {
      headers: {
        "Content-Type": "application/json",
      }});
  }
  getAllProducts(title, page, size) {
    return http.get("/getAllProducts", { params: {title: title, page: page, size: size } });
  }

  getUserProducts(email) {
    return http.get("/getUserProducts", { params: { userEmail: email } });
  }

  getProduct(id) {
    return http.get("/getProduct", { params: { productId: id } });
  }

  getMyRequests(email) {
    return http.get("/myRequests", { params: {userEmail: email } });
  }

  getGivingRequests(email) {
    return http.get("myGivingRequests", { params: {userEmail: email } });
  }

  getTypes() {
    return http.get("/getTypes");
  }

  getUserFavoriteProducts(email) {
    return http.get("/getUserFavoriteProducts", { params: {userEmail: email } } );
  }

  getFavorite(email, id) {
    return http.get("/favorite", { params: { email: email, id: id } });
  }

  deleteFavorite(email, id) {
    return http.delete("/favorite", { params: { email: email, id: id } });
  }

  addFavorite(email, id) {
    return http.post("/addToFavorite", { params: { email: email, id: id } });
  }

  addRequest(email, id) {
    return http.post("/requestBook",null, { params: { userEmail: email, id: id } });
  }

  responseToRequest(email, id, action ,comment) {
    return http.post("/myResponse", null, { params: { userEmail: email, id: id, action: action, comment: comment} });
  }

  deleteProduct(id) {
    return http.delete("/product", { params: { productId: id } });
  }
}

export default new BEAPI();