import React from "react"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import Books from "../pages/Books"
import Home from "../pages/home"
import Requests from "../pages/Requests"
import UserBooks from "../pages/UserBooks"
import Account from "../pages/Account"
import Signup from "../pages/Auth/Signup"
import Login from "../pages/Auth/Login"
import PrivateRoute from "./PrivateRoute"
import ForgotPassword from "../pages/Auth/ForgotPassword"
import UpdateProfile from "../pages/UpdateProfile"
import Book from "../pages/Book"
import AddNewBook from "../pages/AddNewBook"
import UpdateBook from "../pages/UpdateBook"
import UserLayout from "./UserLayout";

const Router = () => {
  return (
      <BrowserRouter>
        <Routes>
          <Route exact path="/signup" element={<Signup />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/forgot-password" element={<ForgotPassword />} />
          <Route exact path="/books" element={<PrivateRoute><Books /></PrivateRoute>} />
          <Route exact path="/books/:title" element={<PrivateRoute><Books /></PrivateRoute>} />
          <Route exact path="/" element={<UserLayout><Home /></UserLayout>} />
          {/* <Route exact path="/" element={<Home />} /> */}
          <Route exact path="/userBooks" element={<PrivateRoute><UserBooks /></PrivateRoute>} />
          <Route exact path="/addBook" element={<PrivateRoute><AddNewBook /></PrivateRoute>} />
          <Route exact path="/updateBook/:id" element={<PrivateRoute><UpdateBook /></PrivateRoute>} />
          <Route exact path="/profile" element={<PrivateRoute><Account /></PrivateRoute>} />
          <Route exact path="/book/:id" element={<PrivateRoute><Book /></PrivateRoute>} />
          <Route exact path="/updateProfile" element={<PrivateRoute><UpdateProfile /></PrivateRoute>} />
          <Route exact path="/applications" element={<PrivateRoute><Requests /></PrivateRoute>} />
        </Routes>
      </BrowserRouter>
  );
};

export default Router;