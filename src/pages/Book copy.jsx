import React, { useState, useEffect } from 'react'
import { useParams, Link, useNavigate } from "react-router-dom"
import ImageGallery from 'react-image-gallery'
import { Container, Row, Col, Button } from 'react-bootstrap';
import GoogleMaps from '../components/GoogleMap'
import API from "../utils/BEAPI"
import { useAuth } from '../contexts/AuthContext'

const Book = () => {
  const navigate = useNavigate();
  const { currentUser }  = useAuth()
  let { id } = useParams();
  const [productData,setProductData]=useState([])
  const [images,setImages]=useState([{original: "https://lh3.googleusercontent.com/ophqpJaO3Mt0BwuZ7yagkN-lCYKP0H_tdLkQE_lA9hy44MtKkg2vLXzqyWI_UUemrJWqsuZyxqKDlKtcMq4EtMyy=w600",thumbnail: "" }])
  const [cord, setCord] = useState({lat: 59.3473154, lon: 18.0732396})
  const [profile, setProfile] = useState({name: "", email: ""})
  const [prof, setProf] = useState([{email: " ", name: "sergiu", phoneNumber: " "}]);

  useEffect(()=>{
    API.getProduct(id)
        .then(res=>{
            console.log('Response from main API: ', res)
            console.log('Response Data: ', res.data)
            setProductData(res.data)
            setCord({lat: parseFloat(res.data.locationLat), lon: parseFloat(res.data.locationLong)})
            setImages(res.data.imagePaths)
            setProfile(res.data.profile)
            API.getProfile(res.data.profile.email)
                .then(res=>{
                    // console.log('Response from main API: ',res)
                    // console.log('Response Data: ',res.data)
                    setProf(res.data)
                })
                .catch(err=>{
                    console.log(err);
                })
        })
        .catch(err=>{
            console.log(err);
        })
    
  },[])


  const googleMap = (
    <div className="google-map-wrapper">
      {(
        <GoogleMaps
          style={{
            width: '300px',
            height: '400px'
          }}
          coord={cord}
          initCoord={cord}
          zoom={10}
        />
      )}
    </div>
  );
  const createPost = () => {
    console.log(id)
        navigate('/updateBook/' + id,
            {
                state: productData
            });
    }

  if (!id || !productData) {
    return (<div>Loading...</div>);
  }

  return (
    <Container>
      <Row>
        <Col md={7}>
        <div>
          <div style={{ textAlign: "center" }}>
            <h2>{productData.title}</h2>
            <div
              style={{
                padding: "0 10px",
              }}
            >
              <ImageGallery style={{
                marginTop: '55px',
                marginBottom: '25px'
              }} items={images} />
            </div>
          </div>
        </div>
        </Col>
        <Col md={{ span: 4, offset: 1 }}>
          <br/>
          {profile.email == currentUser.email && <Button onClick={() => { createPost() }}>Update</Button>}
          {profile.email != currentUser.email && <Button onClick={() => { createPost() }}>Request</Button>}
        <div
              style={{
                marginTop: '55px',
                marginBottom: '25px'
              }}
            >
          <h5>Pickup Location</h5>
          {googleMap}
          </div>
          <h6>You can get this for free from:</h6>
          <h5>Name: {profile.name}</h5>
          <h5>Phone Number: {prof.phoneNumber}</h5>
        </Col>

      </Row>
      <Row>
        <Col md={7}><h3 style={{ textAlign: "center" }}>Description</h3>
        <p>
        {productData.description}
            </p>
        </Col>
        
        
      </Row>

      <Row>
        {/* <Col md={{ span: 3, offset: 2 }}><h5>{productData.description}</h5></Col> */}
        <Col md={{ span: 6, offset: 3 }}></Col>
      </Row>
    </Container>
  );
};

export default Book;