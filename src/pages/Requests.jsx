import React, { useState, useEffect } from 'react';
import { Table, Tab, Tabs, Card, Row, Col, Container, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom"
import API from "../utils/BEAPI"
import { useAuth } from "../contexts/AuthContext"

const Books = () => {
  const {currentUser} = useAuth()
  const navigate = useNavigate()
  const [productGettingData,setProductGettingData]=useState([])
  const [productGivingData,setProductGivingData]=useState([])
  const [selectedFiles, setSelectedFiles] = useState(undefined);

  function siteSelectedCallback(id) {
    navigate('/book/' + id)
  }

  useEffect(()=>{
    API.getMyRequests(currentUser.email)
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setProductGettingData(res.data)

        })
        .catch(err=>{
            console.log(err);
        })
    API.getGivingRequests(currentUser.email)
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setProductGivingData(res.data)

        })
        .catch(err=>{
            console.log(err);
        })
  },[])

  const aproveRequest = (email, id) => {
    API.responseToRequest(email, parseInt(id), 2, "Success")
        .then(res=>{
            console.log('Response from main API: ', res)
            console.log('Response Data: ', res.data)
            setSelectedFiles(res)
        })
        .catch(err=>{
            console.log(err);
        })
      }

  const rejectRequest = (email, id) => {
    API.responseToRequest(email, parseInt(id), 3, "Fail")
        .then(res=>{
            console.log('Response from main API: ', res)
            console.log('Response Data: ', res.data)
            setSelectedFiles(res)
        })
        .catch(err=>{
            console.log(err);
        })
      }

  if (!productGettingData.length) return <h3>Loading...</h3>;

  const handleClick = event => {
    event.currentTarget.disabled = true;
    console.log('button clicked');
  };

  return (
    <div className='page'>
      <Tabs
        defaultActiveKey="profile"
        id="justify-tab-example"
        className="mb-3"
        justify
        >
        <Tab eventKey="home" title="Getting">
          <Container>
            <Row  xs={1}>
              {Array.from(productGettingData).map((prod) => (
                <Table striped bordered hover>
                <tbody>
                  <tr>
                    <td width={250} height={150}>
                      <Card tag="a" onClick={() => siteSelectedCallback(prod.productResp.id)} style={{ cursor: "pointer" }}>
                        <Card.Img variant="top" width={250} height={150} src={prod.productResp.imagePaths[0].original} />
                        <Card.Body>
                          <Card.Title>{prod.productResp.title}</Card.Title>
                        </Card.Body>
                      </Card>
                    </td>
                    <td width={550} height={150}>Date of request: {prod.time}
                      <br/>
                      Status:   {prod.status}
                      
                    </td>
                    <td width={250} height={150}>{ "Name: " + prod.productResp.profile.name}
                    <br/>
                    {prod.status == "Accepted" && "Email: " +  prod.productResp.profile.email}
                    <br/>
                     {prod.status == "Accepted" && "Phone Number: " + prod.productResp.profile.phoneNumber}
                    </td>
                  </tr>
                </tbody>
              </Table>
              ))}
            </Row>
          </Container>
        </Tab>
        <Tab eventKey="profile" title="Giving">
          <Container>
            <Row  xs={1}>
              {Array.from(productGivingData).map((prod) => (
                <Table striped bordered hover>
                <tbody>
                  <tr>
                    <td width={150} height={150}>
                      <Card tag="a" onClick={() => siteSelectedCallback(prod.productResp.id)} style={{ cursor: "pointer" }}>
                        <Card.Img variant="top" width={250} height={150} src={prod.productResp.imagePaths[0].original} />
                      </Card>
                    </td>
                    <td width={350} height={150}>{prod.productResp.title} </td>
                    <td width={350} height={150}>Date of request: {prod.time}  
                    <br/>
                    Requested by: {prod.productResp.profile.name} 
                    </td>
                    <td width={150} height={150}>
                      <Button className="btn btn-black" type="submit" variant="success" disabled={prod.status !== "Pending"} onClick={() => {aproveRequest(prod.productResp.profile.email, prod.productResp.id) && handleClick()}}>Accept</Button>
                      {'  '}
                      <Button className="btn btn-find" variant="danger" onClick={() => {rejectRequest(prod.productResp.profile.email, prod.productResp.id) }}>Reject</Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
              ))}
            </Row>
          </Container>
        </Tab>
        <Tab eventKey="contact" title="Contact" disabled>

        </Tab>
    </Tabs>
    </div>
  );
};

export default Books;