import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Container} from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom"
import API from "../utils/BEAPI"
import { useAuth } from "../contexts/AuthContext"
import Pagination from 'react-bootstrap-4-pagination';

const Books = () => {
  const {currentUser} = useAuth()
  const navigate = useNavigate()
  const [productData,setProductData]=useState([])
 
  const [pageData,setPageData]=useState([])
  let { title } = useParams();

  function siteSelectedCallback(id) {
    navigate('/book/' + id)
  }

  let paginationConfig = {
    totalPages: pageData.totalPages,
    currentPage: pageData.currentPage+1,
    showMax: 5,
    size: "lg",
    threeDots: true,
    prevNext: true,
    circle: true,
    onClick: function(page) {
       console.log(page);
       API.getAllProducts(title, page, 12)
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setProductData(res.data.productResponse)
            setPageData(res.data)
        })
        .catch(err=>{
            console.log(err);
        })
     }
  };
  


  useEffect(()=>{
    API.getAllProducts(title, 1, 12, currentUser.accessToken)
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setProductData(res.data.productResponse)
            setPageData(res.data)
        })
        .catch(err=>{
            console.log(err);
        })

    API.saveOrUpdateProfile(currentUser.email, currentUser.accessToken);
  },[])

  if (!productData.length) return <h3>Loading...</h3>;

  return (
    <div className='page'>
      <br/>
    <Container>
    <Row>
        <Col></Col>
        <Col xs={6}></Col>
        <Col><Pagination {...paginationConfig} /></Col>
      </Row>
    
      <Row  xs={1} sm={2} md={3} xl={4} className="g-4">
        {Array.from(productData).map((prod) => (
          <Col>
            <Card tag="a" onClick={() => siteSelectedCallback(prod.id)} style={{ cursor: "pointer" }}>
              <Card.Img variant="top" width={250} height={250} src={prod.imagePath} />
              <Card.Body>
                <Card.Title>{prod.title}</Card.Title>
                <Card.Text>
                  {prod.description.substring(0, 35)+"..."}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
    </div>
  );
};

export default Books;