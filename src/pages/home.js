import React from 'react'

import { Helmet } from 'react-helmet'
import { Container, Button } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";
import reading from "../assets/reading.bb74eb2f.png"

import stillhave from "../assets/stilhavnmountpleasant-71-1024x683.jpg"
import group from "../assets/group 18.png"
import union from "../assets/union.png"
import './home.css'

const Home = (props) => {
  let navigate = useNavigate(); 
  const routeChange = () =>{ 
    let path = `/books`; 
    navigate(path);
  }

  const routeLogin = () =>{ 
    let path = `/login`; 
    navigate(path);
  }

  return (
    <Container>
    <div className="home-container">
      <Helmet>
        <title>Up Start Finance</title>
        <meta property="og:title" content="Up Start Finance" />
      </Helmet>
      <div className="home-hero hero-container section-container">
        <div className="home-max-width max-width">
          <div className="home-content">
            <span className="home-subtitle before-Heading">
              Book sharing platform
            </span>
            <h1 className="home-title">Why should you join BookSharing</h1>
            <span className="home-description">
              BookSharing is one of the world&apos;s leading book sharing
              platforms. It connects the people who want to give or lend their
              books away to the people who want to read a book without buying
              it. If you want to contribute for a sustainable environment and
              improve the world&apos;s literacy, BookSharing is for you.
            </span>
            <div className="home-container1">
            <button  onClick={routeChange} className="btn btn-find btn-homepage">
                Find Books
              </button>
            <button onClick={routeLogin} className="btn btn-black btn-homepage">
                Join Now
              </button>


            </div>
          </div>
          <div className="home-image">
            <img
              alt="image"
              src={union}
              className="home-graphic-top"
            />
            <img
              alt="image"
              src={group}
              className="home-image1"
            />
            <img
              src={reading}
              alt="image"
              className="home-image2"
            />
          </div>
        </div>
      </div>
      <div className="home-section section-container">
        <div className="home-max-width1 max-width">
          <div className="home-image3">
            <img
              alt="image"
              src={stillhave}
              className="home-hero-image"
            />
          </div>
          <div className="home-content1">
            <span className="home-text">get started</span>
            <h1 className="home-text1">How does BookSharing work?</h1>
            <div className="home-step">
              <div className="home-number">
                <span className="home-text2">1</span>
              </div>
              <div className="home-container2">
                <span className="home-title1">Find your book</span>
                <span className="home-text3">
                  Search the book that you want to read. BookSharing provides a fully
                  comprehensive books from various authors provided in different
                  languages.
                </span>
              </div>
            </div>
            <div className="home-step1">
              <div className="home-number1">
                <span className="home-text4">2</span>
              </div>
              <div className="home-container3">
                <span className="home-title2">Request the book</span>
                <span className="home-text5">
                  Request to lend or get the book from the book&apos;s owner. We
                  let the owner decide to lend or give their books away.
                </span>
              </div>
            </div>
            <div className="home-step2">
              <div className="home-number2">
                <span className="home-text6">3</span>
              </div>
              <div className="home-container4">
                <span className="home-title3">Meet the owner</span>
                <span className="home-text7">
                  Set the appointment to meet the owner to pickup the book. Our
                  platform will only give the owner detail location once the
                  request is approved
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </Container>
  )
}

export default Home
