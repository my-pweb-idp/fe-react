import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom"
import API from "../utils/BEAPI"
import { useAuth } from '../contexts/AuthContext'

const UserBooks = () => {
  const { currentUser }  = useAuth()
  const navigate = useNavigate()
  const [productData,setProductData]=useState([])

  function siteSelectedCallback(id) {
    navigate('/book/' + id)
  }

  useEffect(()=>{
    API.getUserProducts(currentUser.email)
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setProductData(res.data)
        })
        .catch(err=>{
            console.log(err);
        })
  },[])

  if (!productData.length) return <h3>Loading...</h3>;

  return (
    <div className='page'>
    <Container>
      <Row  xs={1} sm={2} md={3} xl={4} className="g-4">
        {Array.from(productData).map((prod) => (
          <Col>
            <Card tag="a" onClick={() => siteSelectedCallback(prod.id)} style={{ cursor: "pointer" }}>
              <Card.Img variant="top" width={250} height={250} src={prod.imagePath} />
              <Card.Body>
                <Card.Title>{prod.title}</Card.Title>
                <Card.Text>
                  {prod.description.substring(0, 35)+"..."}
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
    </div>
  );
};

export default UserBooks;