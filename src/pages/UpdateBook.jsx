import React, {useRef, useState, useEffect} from 'react'
import { useAuth } from '../contexts/AuthContext'
import { Form, Container, Button } from 'react-bootstrap'
import ImagesUpload from "../utils/FileUpload"
import API from "../utils/BEAPI"
import Map from '../components/GoogleMap'
import { useLocation, useParams, useNavigate } from "react-router-dom"

const UpdateBook = () => {
  const navigate = useNavigate()
  const { state } = useLocation();
  let { id } = useParams();
  const titleRef = useRef()
  const descriptionRef = useRef()
  const authorRef = useRef()
  const { currentUser }  = useAuth()
  const [selectedFiles, setSelectedFiles] = useState(undefined);
  const [progressInfos, setProgressInfos] = useState({ val: [] });
  const [message, setMessage] = useState([]);
  const [types, setTypes] = useState([{name: "Transport", description: ""}]);
  const [type, setType] = useState();
  const progressInfosRef = useRef(null)
  const [location, setLocation] = useState({
    lat: state.locationLat,
    lon: state.locationLong
  });
  const [initLocation, setInitLocation] = useState({
    lat: state.locationLat,
    lon: state.locationLong
  });

  const changeLocation = loc => {
    console.log(loc)
    setLocation(loc);
  }

  const getAddress = val => {}

  useEffect(()=>{
    API.getTypes()
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setTypes(res.data)
        })
        .catch(err=>{
            console.log(err);
        })
  },[])

  const selectFiles = (event) => {
    setSelectedFiles(event.target.files);
    setProgressInfos({ val: [] });
  };

  const upload = (idx, file, rand) => {
    let _progressInfos = [...progressInfosRef.current.val];
    return ImagesUpload.upload(currentUser.email,rand,file, (event) => {
      _progressInfos[idx].percentage = Math.round(
        (100 * event.loaded) / event.total
      );
      setProgressInfos({ val: _progressInfos });
    })
      .then(() => {
        setMessage((prevMessage) => ([
          ...prevMessage,
          "Uploaded the file successfully: " + file.name,
        ]));
      })
      .catch(() => {
        _progressInfos[idx].percentage = 0;
        setProgressInfos({ val: _progressInfos });

        setMessage((prevMessage) => ([
          ...prevMessage,
          "Could not upload the file: " + file.name,
        ]));
      });
  };

  const uploadFiles = (rand) => {
    const files = Array.from(selectedFiles);
    
    let _progressInfos = files.map(file => ({ percentage: 0, fileName: file.name }));

    progressInfosRef.current = {
      val: _progressInfos,
    }
    

    const uploadPromises = files.map((file, i) => upload(i, file, rand));
    
    Promise.all(uploadPromises)
    setMessage([]);
  };

  const onChangeType = (type) => {
    setType(type)
  };

  
  function handleSubmit(e) {
    e.preventDefault()
    // const min = 1;
    // const max = 100000;
    // const rand = min + Math.random() * (max - min); 
    API.updateProduct(currentUser.email, id, titleRef.current.value, descriptionRef.current.value, location.lat, location.lon, type,  authorRef.current.value);
    // uploadFiles(rand)
    navigate("/")
  }

  function deleteProduct(e) {
    e.preventDefault()

    API.deleteProduct(id);
    // uploadFiles(rand)
    navigate("/")
  }

  return (
    <div>
       <div style={{ textAlign: "center" }}>
            <h2>Update Post</h2></div>
      <Container>
        <Form onSubmit={handleSubmit} >
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>{console.log("dsadasdas")}{console.log(state)}Title</Form.Label>
            <Form.Control type="name" placeholder="Title Example" ref={titleRef} defaultValue={state.title}
                required />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Description</Form.Label>
            <Form.Control as="textarea" rows={6} placeholder="Description Example" ref={descriptionRef} defaultValue={state.description}
                required />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Author</Form.Label>
            <Form.Control as="textarea" rows={6} placeholder="Author Example" ref={authorRef} defaultValue={state.author}
                required />
          </Form.Group>
          {/* <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Type</Form.Label>
            <Form.Control as="selected" ref={descriptionRef} defaultValue={state.description}
                required >
                  {types.map(opt => (
                  <option value={opt.name}>{opt.name}</option>
                ))}
                  </Form.Control>
              {/* <Form.Select onChange={onChangeType(opt.id)}>
              {types.map(opt => (
                  <option value={opt.name}>{opt.name}</option>
                ))}
              </Form.Select> 
          </Form.Group> */}

          <Form.Group controlId="formBasicSelect">
            <Form.Label>Select Type</Form.Label>
            <Form.Control
              as="select"
              value={type}
              // defaultValue={types[3].name}
              onChange={e => {
                console.log("e.target.value", e.target.value);
                setType(e.target.value);
                
              }}
            >
              {types.map(opt => (
                      <option value={opt.name}>{opt.name}</option>
                    ))}
            </Form.Control>
          </Form.Group>
          <Form.Group controlId="formFileMultiple" className="mb-3">
            {progressInfos && progressInfos.val.length > 0 &&
              progressInfos.val.map((progressInfo, index) => (
                <div className="mb-2" key={index}>
                  <span>{progressInfo.fileName}</span>
                  <div className="progress">
                    <div
                      className="progress-bar progress-bar-info"
                      role="progressbar"
                      aria-valuenow={progressInfo.percentage}
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: progressInfo.percentage + "%" }}
                    >
                      {progressInfo.percentage}%
                    </div>
                  </div>
                </div>
              ))}
              <Form.Label>Multiple files input example</Form.Label>
              <Form.Control type="file" multiple onChange={selectFiles} />
            {message.length > 0 && (
              <div className="alert alert-secondary" role="alert">
                <ul>
                  {message.map((item, i) => {
                    return <li key={i}>{item}</li>;
                  })}
                </ul>
              </div>
            )}
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Select on map</Form.Label>
            <div className="subtitle">Location</div>
            <Map
              style={{
                width: '100%',
                height: '400px',
                position: 'relative',
                border: '1px solid #d1d5da',
                borderRadius: '6px',
                overflow: 'hidden'
              }}
              zoom={5}
              coord={location}
              getCoord={changeLocation}
              initCoord={initLocation}
              autocomplete={true}
              mapClick={true}
              getAddress={getAddress}
            />
          </Form.Group>
          <Button variant="primary" type="submit" >
            Update
          </Button>

        </Form>
        <br/>
        <Button variant="danger" type="delete" onClick={deleteProduct} >
            Delete 
          </Button>
      </Container>
    </div>
  )
}

export default UpdateBook;
