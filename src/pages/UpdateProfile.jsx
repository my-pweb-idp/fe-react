import React, {useRef, useState, useEffect} from 'react'
import { Form, Button, Card, Container, Alert } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext'
import { Link, useNavigate} from "react-router-dom"
import API from "../utils/BEAPI"

export default function UpdateProfile() {
  const emailRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()
  const displayedNameRef = useRef()
  const phoneNumberRef = useRef()
  const { currentUser, updateUserEmail, updateUserPassword, updateUserProfile} = useAuth()
  const [error, setError] = useState('')
  const [prof, setProf] = useState([{email: " ", name: "sergiu", phoneNumber: " "}]);
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()
  
  useEffect(()=>{
    API.getProfile(currentUser.email)
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setProf(res.data)
        })
        .catch(err=>{
            console.log(err);
        })
  },[])

  function handleSubmit(e) {
    e.preventDefault()

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError('Password do not match')
    }

    const promises = []
    setError('')
    setLoading(true)

    if (emailRef.current.value !== currentUser.email) {
      promises.push(updateUserEmail(emailRef.current.value))
    }

    if (passwordRef.current.value) {
      promises.push(updateUserPassword(passwordRef.current.value))
    }

    if (displayedNameRef.current.value !== currentUser.displayName) {
      promises.push(updateUserProfile(displayedNameRef.current.value, null))
    }

    promises.push(API.updateProfile(currentUser.email, phoneNumberRef.current.value, displayedNameRef.current.value, currentUser.accessToken))
    Promise.all(promises)
      .then(() => {
        navigate('/profile')
      })
      .catch(() => {
        setError('Failed to update account')
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <Container 
      className='d-flex align-items-center justify-content-center'
      style={{minHeight: "100vh"}}>
      <div className='w-100' style={{ maxWidth: '500px'}}>
        <Card>
          <Card.Body>
            <h2 className='text-center mb-4'>Update Profile</h2>
            {error && <Alert variant="danger">{error}</Alert>}
            <Form onSubmit={handleSubmit}>
              <Form.Group id="displayedName">
                <Form.Label>Displayed Name</Form.Label>
                <Form.Control type= "displayedName" ref={displayedNameRef} 
                required defaultValue={prof.name} />
              </Form.Group>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control type= "email" ref={emailRef} 
                required defaultValue={prof.email} />
              </Form.Group>
              <Form.Group id="email">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control type= "phoneNumber" ref={phoneNumberRef} 
                required defaultValue={prof.phoneNumber} />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label>Password</Form.Label>
                <Form.Control type= "password" ref={passwordRef} 
                placeholder='Leave blank to keep the same' />
              </Form.Group>
              <Form.Group id="password-confirm">
                <Form.Label>Password Confirmation</Form.Label>
                <Form.Control type= "password" ref={passwordConfirmRef} 
                placeholder='Leave blank to keep the same' />
              </Form.Group>
              <Button disabled={loading} className="w-100 mt-4" type="submit">
                Update
              </Button> 
            </Form>
          </Card.Body>
        </Card>
        <div className='w-100 text-center mt-2'>
          <Link to="/profile">Cancel</Link>
        </div>
      </div>
    </Container>
  )
}
