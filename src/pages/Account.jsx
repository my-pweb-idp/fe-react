import React, {useRef, useState, useEffect} from "react";
import { useAuth } from '../contexts/AuthContext'
import { Button, Card, ListGroup, ListGroupItem, Container} from 'react-bootstrap'
import { useNavigate } from "react-router-dom"
import API from "../utils/BEAPI"

const Account = () => {
  
  const { currentUser }  = useAuth()
  const navigate = useNavigate()

  const [prof, setProf] = useState([{email: " ", name: "sergiu", phoneNumber: " "}]);

  useEffect(()=>{
    API.getProfile(currentUser.email)
        .then(res=>{
            // console.log('Response from main API: ',res)
            // console.log('Response Data: ',res.data)
            setProf(res.data)
        })
        .catch(err=>{
            console.log(err);
        })
  },[])

  const routeChange = () =>{ 
    navigate('/updateProfile')
  }
  
  return (
    <Container 
      className='d-flex align-items-center justify-content-center'
      style={{minHeight: "100vh"}}>
      <div className='w-100' style={{ maxWidth: '500px'}}>
        <Card style={{ width: '18rem' }}>
          <Card.Img variant="top" src={currentUser.photoURL} />
          <Card.Body>
            <Card.Title>Account Information</Card.Title>
          </Card.Body>
          <ListGroup className="list-group-flush">
            <ListGroupItem><strong>Displayed Name:</strong> {currentUser.displayName}</ListGroupItem>
            <ListGroupItem><strong>Email:</strong> {currentUser.email}</ListGroupItem>
            <ListGroupItem><strong>Phone Number:</strong>{prof.phoneNumber}</ListGroupItem>
          </ListGroup>
          <Card.Body>
            <Button variant="primary" onClick={routeChange}>Update Profile</Button>
          </Card.Body>
        </Card>
      </div>
    </Container>
    // <div className="account-page">
    //   <h1>My Profile</h1>
    //   <div className="user-information">
    //     {/* <Avatar avatarUrl={currentUser.photoURL} /> */}
    //     <div className="user-info-container">
    //       <span className="username">Hey, I am {currentUser.displayName} </span>
    //       <span className="info-item">
    //         <FaEnvelope />
    //         <span>{currentUser.email}</span>
    //       </span>
    //       <span className="info-item">
    //         {/* <FontAwesomeIcon icon="phone" /> */}
    //         <span>
    //           {currentUser.providerData.phoneNumber !== null ? (
    //             currentUser.providerData.phoneNumber
    //           ) : (
    //             <i>Tell us your phone number</i>
    //           )}
    //         </span>
    //       </span>
    //       {/* <span className="info-item">
    //         <FontAwesomeIcon icon="home" />
    //         <span>{location}</span>
    //       </span> */}
    //       {/* <span className="info-item">
    //         <FontAwesomeIcon icon="birthday-cake" />
    //         <span>{age !== null ? age : <i>Tell us your age</i>}</span>
    //       </span> */}
    //       {/* <Link to={ROUTES.EDIT_PROFILE} className="edit-profile">
    //         Edit Profile
    //       </Link> */}
    //     </div>
    //   </div>

    //   <div className="line-break" />
    //   <div className="my-books-section">
    //     <h2>My Books</h2>
    //     {/* <Link className="btn btn-add-book account" to={ROUTES.ADD_BOOK}>
    //       <span>Add Book</span>
    //     </Link> */}
    //   </div>
    //   {/* {!_.isEmpty(myBooks) ? (
    //     <SearchResults books={myBooks.reverse()} accountPage={true} />
    //   ) : (
    //     <p className="no-results">You haven't added any books yet.</p>
    //   )} */}
    // </div>
  );
};

export default Account;
