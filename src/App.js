import './App.css';
import React from "react";
import Router from "./utils/Routing";
import { AuthProvider } from "./contexts/AuthContext"


function App() {
  return (
    <AuthProvider>
      <Router />
    </AuthProvider>
    // <Router />
  );
}

export default App;
